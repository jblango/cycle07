import os
import wmi
import win32con
import win32security
import sys
import win32api
import ntsecuritycon as con

#The GetTokenInformation function retrieves a specified type of information about an access token.
#The calling process must have appropriate access rights to obtain the information.
#https://msdn.microsoft.com/en-us/library/windows/desktop/aa446671(v=vs.85).aspx
#https://msdn.microsoft.com/en-us/library/windows/desktop/aa379633(v=vs.85).aspx 

# The following table lists the values for file and directory access rights in the AccessMask property.
# This property is a bitmap.
ACCESSMASKSPROPERTY = ['FILE_READ_DATA', 'FILE_LIST_DIRECTORY', 'FILE_WRITE_DATA', 'FILE_ADD_FILE', 
                     'FILE_APPEND_DATA', 'FILE_ADD_SUBDIRECTORY', 'FILE_CREATE_PIPE_INSTANCE',
                     'FILE_READ_EA', 'FILE_WRITE_EA', 'FILE_EXECUTE', 'FILE_TRAVERSE', 'FILE_DELETE_CHILD', 
                     'FILE_READ_ATTRIBUTES', 'FILE_WRITE_ATTRIBUTES', 'FILE_ALL_ACCESS', 'FILE_GENERIC_READ',
                     'FILE_GENERIC_WRITE', 'FILE_GENERIC_EXECUTE'] 

# Access Control Entries (ACE)
#https://msdn.microsoft.com/en-us/library/windows/desktop/aa374919(v=vs.85).aspx
ACETYPES = ['ACCESS_ALLOWED_ACE_TYPE','ACCESS_ALLOWED_CALLBACK_ACE_TYPE','ACCESS_ALLOWED_CALLBACK_OBJECT_ACE_TYPE',
            'ACCESS_ALLOWED_OBJECT_ACE_TYPE','ACCESS_DENIED_ACE_TYPE','ACCESS_DENIED_CALLBACK_ACE_TYPE',
            'ACCESS_DENIED_CALLBACK_OBJECT_ACE_TYPE','ACCESS_DENIED_OBJECT_ACE_TYPE','ACCESS_MAX_MS_ACE_TYPE',
            'ACCESS_MAX_MS_V2_ACE_TYPE','ACCESS_MAX_MS_V3_ACE_TYPE','ACCESS_MAX_MS_V4_ACE_TYPE','ACCESS_MAX_MS_OBJECT_ACE_TYPE',
            'ACCESS_MIN_MS_ACE_TYPE','ACCESS_MIN_MS_OBJECT_ACE_TYPE','SYSTEM_ALARM_ACE_TYPE','SYSTEM_ALARM_CALLBACK_ACE_TYPE',
            'SYSTEM_ALARM_CALLBACK_OBJECT_ACE_TYPE','SYSTEM_ALARM_OBJECT_ACE_TYPE','SYSTEM_AUDIT_ACE_TYPE','SYSTEM_AUDIT_CALLBACK_ACE_TYPE',
            'SYSTEM_AUDIT_OBJECT_ACE_TYPE','SYSTEM_MANDATORY_LABEL_ACE_TYPE','ACCESS_ALLOWED_COMPOUND_ACE_TYPE','SYSTEM_AUDIT_CALLBACK_OBJECT_ACE_TYPE',
            'ACCESS_MAX_MS_V5_ACE_TYPE']

#https://msdn.microsoft.com/en-us/library/windows/desktop/aa379626(v=vs.85).aspx
TOKENTYPEVALUE = ['Unknown','Token Primary','Token Impersonation']
TOKENMANDATORYPOLICY = ['TOKEN_MANDATORY_POLICY_OFF',
                         'TOKEN_MANDATORY_POLICY_NO_WRITE_UP',
                         'TOKEN_MANDATORY_POLICY_NEW_PROCESS_MIN',
                         'TOKEN_MANDATORY_POLICY_VALID_MASK']
#Token evelation
TOKENELEVATIONTYPE = ['Unknown','TokenElevationTypeDefault','TokenElevationTypeFull','TokenElevationTypeLimited']

#Security Level
SECURITYIMPERSONATIONLEVEL = ['SecurityAnonymous','SecurityIdentification','SecurityImpersonation','SecurityDelegation']

def logAuditing(msg):
    fd = open("logAuditingCreation.txt","at")
    s = str(msg)
    fd.write(s)
    fd.write("\n")
    fd.close()
    return
    
def getAccessMask(accessMask):
    for entryMask in ACCESSMASKSPROPERTY:
        entryMaskAttr = getattr(con, entryMask)
        if (entryMaskAttr & accessMask) == entryMaskAttr:
            yield entryMask
            
# get the the acces control entry for a give ACE 
def getAceTypes(aceType):
    for entry in ACETYPES:
        if getattr(con, entry) == aceType:
            yield entry

     
def processMonitor(winManInterface):
    print("\n........Process monitoring....\n")
    print("Image Name\tPID\tUser\tExecutatble\tCreation Date\t Timestamp\n")
    print("==========\t===\t====\t============\t===========\t===========\n")
    
    win32ProcessWatchCreation = winManInterface.Win32_Process.watch_for("creation")
      
    while True:

        try:
            processExecutablePath = ""
            processWatcher = win32ProcessWatchCreation()
           
            processOwner = processWatcher.GetOwner()
            processID = processWatcher.ProcessId
            msg1 = processWatcher.Name + "\t"+ str(processWatcher.ProcessId)
            
            msg2 = "(\tOwner-Unknown \t)"
            try:
                msg2 = "\t(" + processOwner[0] + " [" + processOwner[2] + "])\t"
            except:
                msg2 = "(\tOwner-Unknown \t)"
                
            msg3 = "Unknown Path\t"
            try:
                msg3 = processWatcher.ExecutablePath + "\t"
                processExecutablePath = processWatcher.ExecutablePath
                
            except:
                msg3 = "Unknown Path\t"

            msg4 = "Creation Date Unknown\t"
            try:
                msg4 = str(processWatcher.CreationDate) + "\t"
                
            except:
                msg4 = "Creation Date Unknown\t"

            msg5 = "NO timestamp"
            try:
                msg5 = str(processWatcher.timestamp)
            except:
                msg5 = "NO timestamp"
                
  
            msgAll = str(msg1) + str(msg2) + str(msg3) + str(msg4) + str(msg5) 
            
            print(msgAll)
            logAuditing(msgAll)
            #displayFileAccessControlList(processExecutablePath)
            displayProcessPrivilege(processID)
        except KeyboardInterrupt:
            print("\nBreak out -[Creation] \n")
            sys.exit()
        except:
            print("\ncontinue... [Creation] \n")
            pass
        
def listRunningProcess(winInterface):
    print("\n*******************************************************************")
    print(".......Printing running processes.....")
    print("*******************************************************************\n")
    for process in winInterface.Win32_Process ():
        msg1 = "Process Name: " +process.Name + "\t PID: "+ str(process.ProcessId)
        processPathName = ""
        try:
            processPathName = process.ExecutablePath
        except:
            print("PATH_NOT_KNOW")
        print(msg1)
        logAuditing(msg1)
        displayFileAccessControlList(processPathName)
        print('<------------------------------------>')
        
    print("*******************************************************************\n")

def getTokenUser(currentProcessToken):
    try:
        print('[+]Token User:')
        tokenUser = win32security.GetTokenInformation(currentProcessToken, win32security.TokenUser) 
        print('\t',tokenUser[0])
        print('\t',tokenUser[1])
    except:
        print('\tUnknown Token User')
        
def getTokenImpersonationLevel(currentProcessToken):
    try:
        
        print('[+]Token Impersonation Level: ')
        tokenImpersonationLevel = win32security.GetTokenInformation(currentProcessToken, win32security.TokenImpersonationLevel)

        print('\t',SECURITYIMPERSONATIONLEVEL[tokenImpersonationLevel])
    except:
        print('\tUnknown Impersonation Level')
        
def displayProcessPrivilege(processID):
    try:
        print('+[Retrieving privileges]:')
        processHandle = win32api.OpenProcess(win32con.PROCESS_QUERY_INFORMATION, False,processID)

        processToken = win32security.OpenProcessToken(processHandle,win32con.TOKEN_QUERY)

        print('TokenPrivileges')
        processPrivileges = win32security.GetTokenInformation(processToken,win32security.TokenPrivileges)


        print('[+]Token Type:')
        processTokenType = win32security.GetTokenInformation(processToken, win32security.TokenType)
        print('\t',TOKENTYPEVALUE[processTokenType])
       
        getTokenUser(processToken)
        getTokenImpersonationLevel(processToken)
        
        print('[+]TokenElevation:')
        tokenElevation = win32security.GetTokenInformation(processToken, win32security.TokenElevation)
        if tokenElevation == 0:
            print('\tToken has NO elevated privileges')
        else:
            print('\tToken has elevated privileges')
        
        print('[+]TokenElevationType:')
        tokenElevationType = win32security.GetTokenInformation(processToken, win32security.TokenElevationType)
        print('\t',TOKENELEVATIONTYPE[tokenElevationType])
        
        print('[+]TokenVirtualizationEnabled:')
        tokenVirtualizationEnabled = win32security.GetTokenInformation(processToken, win32security.TokenVirtualizationEnabled)
        if tokenVirtualizationEnabled == 0:
            print('\tToken Virtualization NOT Enabled')
        else:
            print('\tToken Virtualization Enabled')
        
        print('[+]TokenHasRestrictions:')
        tokenHasRestrictions = win32security.GetTokenInformation(processToken, win32security.TokenHasRestrictions)
        if tokenVirtualizationEnabled == 0:
            print('\tToken has NOT been filtered')
        else:
            print('\tToken has been filtered')
        
        print('[+]TokenMandatoryPolicy:')
        tokenMandatoryPolicy = win32security.GetTokenInformation(processToken, win32security.TokenMandatoryPolicy)
        print('\t',TOKENMANDATORYPOLICY[tokenMandatoryPolicy])
               
        privEnabledList = []
        privDisabledList = []
        privilegeList = ""
        for privilege in processPrivileges:          
            print(privilege)
            priv = win32security.LookupPrivilegeName(None,privilege[0])
            if (privilege[1] == 3):
                privEnabledList.append(priv)
            else:
                privDisabledList.append(priv)


        print('[+] Enabled Privilege(s):')
        for privEnabled in privEnabledList:
            print ('\t',privEnabled)

        print('[+] Disabled Privilege(s): ')
        for privDisabled in privDisabledList:
            print ('\t',privDisabled)

    except:
        print('**Privilege Error**') 

    print('----------------------------------------')

    
def displayFileAccessControlList(fileName):
    try:
        #msgData = []
        securityDescriptor = win32security.GetFileSecurity(fileName, win32security.DACL_SECURITY_INFORMATION)

        securityDescriptorDacl = securityDescriptor.GetSecurityDescriptorDacl()     

        aceCount = securityDescriptorDacl.GetAceCount()
        
        print('File: ', fileName)
        print('Access Control Entries : ('+str(aceCount)+')')
        msgData1 = 'File: '+fileName
        logAuditing(msgData1)
        msgData2 = 'Access Control Entries : ('+str(aceCount)+')'
        logAuditing(msgData2)
        
        for counter in range(0, aceCount):
            (aceType, aceFlag), accessMask, userSID = securityDescriptorDacl.GetAce(counter)
            
            user, group, userType = win32security.LookupAccountSid('', userSID)

            msgData1 = str('User: {}\\{}').format(group, user)
            msgData2 = str('ACE Type ({}):').format(aceType), '; '.join(getAceTypes(aceType))
            msgData3 = str('Access Mask ({}):').format(accessMask), ' | '.join(getAccessMask(accessMask))
            
           
            print(msgData1)
            print('[+}',msgData2)
            print('[+]',msgData3)
            print('')
            
            logAuditing(msgData1)
            logAuditing(msgData2)
            logAuditing(msgData3)
            #logAuditing(msgData4)
    except:
        print('FILE_ACCESS_FAILED - File Path Not Available')
        
def main():
    Cycle = 7
    print('*****Cycle 07****')
    winInterface = wmi.WMI()
        
    if Cycle == 7:
        processMonitor(winInterface)    
    else:
        if len(sys.argv)  == 2:
            #type> python pmPriv.py fileName
            fileName = sys.argv[1]
            print('Processing file...['+fileName+']')
            print('')
            displayFileAccessControlList(fileName)
        else:    
            #listRunningProcess(winInterface)
            processMonitor(winInterface)
    print('******')

    
if __name__ == "__main__":
    main()
